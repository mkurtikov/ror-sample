require 'spec_helper'

describe User, type: :model do
  subject(:user) { build :user }

  describe 'validations' do
    it { is_expected.to validate_presence_of :email   }
    it { is_expected.to validate_uniqueness_of :email }
    it { is_expected.to respond_to :password_digest   }
  end

  describe 'associations' do
    it { is_expected.to have_many :sessions }
  end

  describe ''
end
